import React from 'react'

interface IntroduceSectionProps {}
export const IntroduceSection: React.FC<IntroduceSectionProps> = props => {
  return (
    <div className="section section1">
      <h3>Section 1</h3>
    </div>
  )
}
